package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/flight")
@RestController
public class FlightController {
	@Autowired
	UserService userService;
	@PostMapping("/inventory")
	private String flight(@RequestBody flightdetails flightdetails) {
		userService.save(flightdetails);
		System.out.println("Flight Name: "+ flightdetails.getFlightname());
		return "inventory called";
	}
	@PostMapping("/search/{id}")
	private String search(@RequestBody booking username, @PathVariable Integer id) {
		userService.find(username);
		System.out.println("emailid: "+username.getEmailid());
		return "search called";
	
	}
	@PostMapping("/booking")
	private String booking(@RequestBody booking Booking) {
		userService.save(Booking);
		System.out.println("username: "+Booking.getUsername());
		return "booking";
	}
	@GetMapping("/ticket/{pnr}")
	private String ticket(@RequestBody ticket ticket, @PathVariable Integer pnr) {
		System.out.println("Ticket PNR: "+pnr);
		return "pnr called";
	}
	
}
