package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	UserRepository repository;
	@Autowired
	BookingRepository bookingrepo;
	public void save(flightdetails flightdetails) {
		repository.save(flightdetails);
		System.out.println(flightdetails);
	}
	public void save(booking Booking) {
		bookingrepo.save(Booking);
		System.out.println(Booking);
	}
	public void find(booking username) {
		List<booking> result = bookingrepo.findAll();	
		System.out.println(result);
	}
}