package com.example.demo;

import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class booking {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) Integer id;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	private String username;
	private String emailid;
	private long noofseats;
	private String passengername;
	private String passengergender;
	private String passengerage;
	private String meal;
	private String seatNo;
	
    Random rand = new Random();
    int pnr = rand.nextInt(1000);
    
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public  String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public long getNoofseats() {
		return noofseats;
	}
	public void setNoofseats(long noofseats) {
		this.noofseats = noofseats;
	}
	public String getPassengername() {
		return passengername;
	}
	public void setPassengername(String passengername) {
		this.passengername = passengername;
	}
	public String getPassengergender() {
		return passengergender;
	}
	public void setPassengergender(String passengergender) {
		this.passengergender = passengergender;
	}
	public String getPassengerage() {
		return passengerage;
	}
	public void setPassengerage(String passengerage) {
		this.passengerage = passengerage;
	}
	public String getMeal() {
		return meal;
	}
	public void setMeal(String meal) {
		this.meal = meal;
	}
	public String getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}
	public int getPnr() {
		return pnr;
	}
	public void setPnr(int pnr) {
		this.pnr = pnr;
	}

 
}

