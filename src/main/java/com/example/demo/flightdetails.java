package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class flightdetails {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String flightnumber;
    private String flightname;
    private String sourcedestination;
    private String postdestination;
    private String startdatetime;
    private String enddatetime;
    private String scheduleddays;
    private String instrumentused;
    private long businessclassseats;
    private long nonbusinessclassseats;
    private String meal;
    private long totalcost;
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFlightnumber() {
		return flightnumber;
	}
	public void setFlightnumber(String flightnumber) {
		this.flightnumber = flightnumber;
	}
	public String getFlightname() {
		return flightname;
	}
	public void setFlightname(String flightname) {
		this.flightname = flightname;
	}
	public String getSourcedestination() {
		return sourcedestination;
	}
	public void setSourcedestination(String sourcedestination) {
		this.sourcedestination = sourcedestination;
	}
	public String getPostdestination() {
		return postdestination;
	}
	public void setPostdestination(String postdestination) {
		this.postdestination = postdestination;
	}
	public String getStartdatetime() {
		return startdatetime;
	}
	public void setStartdatetime(String startdatetime) {
		this.startdatetime = startdatetime;
	}
	public String getEnddatetime() {
		return enddatetime;
	}
	public void setEnddatetime(String enddatetime) {
		this.enddatetime = enddatetime;
	}
	public String getScheduleddays() {
		return scheduleddays;
	}
	public void setScheduleddays(String scheduleddays) {
		this.scheduleddays = scheduleddays;
	}
	public String getInstrumentused() {
		return instrumentused;
	}
	public void setInstrumentused(String instrumentused) {
		this.instrumentused = instrumentused;
	}
	public long getBusinessclassseats() {
		return businessclassseats;
	}
	public void setBusinessclassseats(long businessclassseats) {
		this.businessclassseats = businessclassseats;
	}
	public long getNonbusinessclassseats() {
		return nonbusinessclassseats;
	}
	public void setNonbusinessclassseats(long nonbusinessclassseats) {
		this.nonbusinessclassseats = nonbusinessclassseats;
	}
	public String getMeal() {
		return meal;
	}
	public void setMeal(String meal) {
		this.meal = meal;
	}
	public long getTotalcost() {
		return totalcost;
	}
	public void setTotalcost(long totalcost) {
		this.totalcost = totalcost;
	}
	public long getNoofrows() {
		return noofrows;
	}
	public void setNoofrows(long noofrows) {
		this.noofrows = noofrows;
	}
	private long noofrows;
	
}